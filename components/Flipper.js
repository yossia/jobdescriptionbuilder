import React, { Component, PropTypes } from 'react';

export default class Flipper extends Component {

  render() {
    const { line, builder} = this.props; 

    const good = builder.getIn(['flipperWords', 'good', Math.floor((Math.random() * 3))]);
    const casual = builder.getIn(['flipperWords', 'casual', Math.floor((Math.random() * 3))]);

    return (
      <li>
        {eval("`" + line + "`")}
      </li>);
  }

}

Flipper.propTypes = {
  line: PropTypes.string.isRequired,
  builder: PropTypes.object.isRequired
}

