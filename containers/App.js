import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import MainSection from '../components/MainSection';
import * as PreviewActions from '../actions/PreviewActions';
import * as BuilderActions from '../actions/BuilderActions';

class App extends Component {

  render() {
    const { preview, builder , actions } = this.props;
    return (
      <MainSection preview={preview} 
                   builder={builder}  
                   actions={actions} />
    );  
  }
}

function mapState(state) {
  return {
    preview: state.preview,
    builder: state.builder,
  };
}

function mapDispatch(dispatch) {
  return {
    actions: bindActionCreators({...PreviewActions, ...BuilderActions}, dispatch)
  };
}

export default connect(mapState, mapDispatch)(App);