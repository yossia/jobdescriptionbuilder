import {List, Map, fromJS} from 'immutable';
import * as ActionType from '../constants/ActionTypes';

const initialState = fromJS({
    title: "Driver",
    location: "Greater Manchester",
    aboutCompany: "Our company is the best",
    aboutJob: "You will drive kids to school", 
    skills: [ "id2","id3"],
    benefits: ["id1", "id2", "id3"]
});

function updateArrayField(state, key, value){
  const index = state.get(key).indexOf(value);
  if( index > -1){
    return state.deleteIn([key,index]);
  }else{
    return state.set(key, state.get(key).push(value));
  }
}

export default function preview(state = initialState, action) {
  switch (action.type) {

    case ActionType.UPDATE_STRING_FIELD:
      return state.set(action.key, action.value);

    case ActionType.UPDATE_ARRAY_FIELD:
      return updateArrayField(state, action.key, action.value);

    case ActionType.REQUEST_SKILLS:
      return state.set('skills', fromJS([]));

    default:
      return state;
  }
}