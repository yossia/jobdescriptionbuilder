import * as types from '../constants/ActionTypes';

export function requestSkills(jobTitle) {
  return {
    type: types.REQUEST_SKILLS,
    jobTitle: jobTitle
  };
}

export function receiveSkills(allSkills) {
  return {
    type: types.RECIEVE_SKILLS,
    allSkills: allSkills
  };
}

//api call is a side effect, so run via thunk middleware 
export function fetchSkills(jobTitle) {

  return dispatch => {
    dispatch(requestSkills(jobTitle))

    //usually there will be a promise from an api call
    //instead we just return hard coded data.
    const allSkills = {
      Policeman: {
        id1: {
          id: "id1", 
          label: "Driving license", 
          template: "A full driver license"
        },
        id2: {
          id: "id2", 
          label: "Above 6ft tall", 
          template: "Should be a tall person above 6ft"
        },
        id3: {
          id: "id3", 
          label: "People skills", 
          template: "Should be ${good} in dealing with the public"
        }
      },
      default: {
        id1: {
          id: "id1", 
          label: "Driving license", 
          template: "A full driver license"
        },
        id2: {
          id: "id2", 
          label: "Clean driving record", 
          template: "A driving record free from infringements"
        },
        id3: {
          id: "id3", 
          label: "Presentation skills", 
          template: "Demonstrated ${good} presentation skills"
        }
      }
    }

    if (jobTitle === 'Policeman'){
      dispatch(receiveSkills(allSkills.Policeman));
    }else{
      dispatch(receiveSkills(allSkills.default));
    }
      

  }
}