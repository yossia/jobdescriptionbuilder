import {List, Map, fromJS} from 'immutable';
import * as ActionType from '../constants/ActionTypes';

const initialState = fromJS({
  isFetchingSkills: false,
  jobTitles: ["Engineer", "Teacher", "Policeman", "Lawyer", "Driver"],
  flipperWords: {
    casual: ["Casual", "Relaxed" ,"Dynamic"],
    good: ["great", "amazing", "fantastic"]
  },
  allSkills: {
    id1: {
      id: "id1", 
      label: "Driving license", 
      template: "A full driver license"
    },
    id2: {
      id: "id2", 
      label: "Clean driving record", 
      template: "A driving record free from infringements"
    },
    id3: {
      id: "id3", 
      label: "Presentation skills", 
      template: "Demonstrated ${good} presentation skills"
    }
  },
  allBenefits: {
    id1: {
      id: "id1", 
      label: "Discount meals", 
      template: "Discount meals"
    },
    id2: {
      id: "id2", 
      label: "Work environment", 
      template: "${casual} work environment and ${good} views"
    },
    id3: {
      id: "id3", 
      label: "Growth opportunities", 
      template: "Opportunities to grow within the industry"
    }
  },
});

function upadateAllSkills(state, allSkills){
  return state.set('allSkills', fromJS(allSkills)).
         set('isFetchingSkills', false);
}

export default function builder(state = initialState, action) {
  switch (action.type) {

    case ActionType.REQUEST_SKILLS:
      return state.set('isFetchingSkills', true);

    case ActionType.RECIEVE_SKILLS:
      return upadateAllSkills(state, action.allSkills);

    default:
      return state;
  }
}