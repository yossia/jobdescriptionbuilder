import { combineReducers } from 'redux';
import preview from './preview';
import builder from './builder';

const rootReducer = combineReducers({
  preview,
  builder
});

export default rootReducer;