## Job Description builder ##

my npm version 3.5.2

my node version 4.2.0

please run `npm install` to install dependencies

then `npm run start` to start the dev server

see the app at `http://localhost:3010`

### Stack ###
- react 
- redux
- material-ui
- immutable.js
- babel
- webpack

### Structure ###
- /: root files for html, js, css and build configuration
- Actions: the command functions, generates command objects for the store. 
- Components: React components for display, contain specific component styling and any rendering related functions.
- Constants: constants
- Containers: High level layout and hooking of react to redux
- reducers: Actual functions returning new store object (immutable) in reaction to commands, without side effects.
- store: redux store init

### Notes ###
- The app starts with some defaults to imitate retrivieng of a saved job description
- Job Title auto complete is case sensitive, the first job title letter is capital
- I haven't implemented the alternatives to missing location, so it is only an input field
- On changing title the skills options change, for the sake of demonstration I got dummy data only for Policeman, the rest get the same default skill list
- API calls are a side effect so I demonstrated retrieving it via middleware using thunk, usually usefull for promises or any side effect type of operations. This is part of redux conventions.
- Textarea boxes are multiline and will grow as you type more lines of text
- Randomizing flippers works for every change in display, not ideal but I don't have time to fix it, is shouldn't be too difficult though
- Since we don't have an API I didn't implement the save, but the data is saved into the preview object.
- I didn't split the screen to many components, mainly because most of the functionality there was one liners, but for the sake of demonstration I have created a separate component for the flipper. 


