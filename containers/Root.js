import React, { Component } from 'react';
import { Provider } from 'react-redux';
import injectTapEventPlugin from "react-tap-event-plugin";
import App from './App';
import DevTools from './DevTools';
import '../index.css';

//Needed for onTouchTap
//Can go away when react 1.0 release
//Check this repo:
//https://github.com/zilverline/react-tap-event-plugin
injectTapEventPlugin();

export default class Root extends Component {
  render() {
    const { store } = this.props;
    return (
      <Provider store={store}>
        <div>
          <App />
        </div>
      </Provider>
    );
  }
}