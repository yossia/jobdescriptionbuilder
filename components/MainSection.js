import React, { Component, PropTypes } from 'react';
import Flipper from '../components/Flipper';
import AutoComplete from 'material-ui/lib/auto-complete';
import TextField from 'material-ui/lib/text-field';
import Checkbox from 'material-ui/lib/checkbox';
import RaisedButton from 'material-ui/lib/raised-button';
import Divider from 'material-ui/lib/divider';
import ThemeManager from 'material-ui/lib/styles/theme-manager';
import MaterialTheme from '../constants/MaterialTheme';


export default class MainSection extends Component {

  getChildContext() {
    return {
      muiTheme: ThemeManager.getMuiTheme(MaterialTheme)
    };
  }

  render() {
    const { preview, builder, actions} = this.props; 

    const layoutStyle = {
      display: 'flex',
      justifyContent: 'space-around'
    }

    const previewStyle = {
      backgroundColor: "#F1F6F9",
      width: '40%',
      padding: '2em',
      marginTop: '4em'
    }

    const builderStyle = {
      width: '40%',
      padding: '0 2em 2em 2em'
    }

    const buttonStyle = {
      marginRight: '1em',
      marginBottom: '1em',
      boxShadow: 'none',
      border: '1px solid #C9CED4',
      color: '#4C5C71'
    }

    const buttonLabelStyle = {
      textTransform: 'none',
    }

    const inputStyle = {
      border: '1px solid #C9CED4', 
      borderRadius: '2px',
      paddingLeft: '1%',
      paddingRight: '1%',
      width: '98%'
    }

    return (
      <div>
        <div style={layoutStyle}>
          <div style={builderStyle}>
            <h1>Add new job</h1>  
            <h3>Job Title</h3>
            <AutoComplete dataSource={builder.get('jobTitles').toJS()} fullWidth={true}
                          hintText="start typing the job title" searchText={preview.get('title')}
                          onNewRequest={(title) => { actions.updateStringField('title', title); 
                                                     actions.fetchSkills(title)}} />

            <h3>Location</h3>
            <TextField defaultValue={preview.get('location')} fullWidth={true}
                       onBlur={(e) => { actions.updateStringField('location', e.target.value); }} /> 

            <h3>Choose the skills you consider to be essential</h3>
            {builder.get('allSkills').toList().map((item, index) => 
              <RaisedButton key={index} label={item.get('label')} style={buttonStyle} labelStyle={buttonLabelStyle}
                        secondary={preview.get('skills').includes(item.get('id'))}
                        onTouchTap={() => {actions.updateArrayField('skills', item.get('id'))}}/>
            )}
            <Divider />
            <h3>What benefits do you offer?</h3>
            {builder.get('allBenefits').toList().map((item, index) => 
              <RaisedButton key={index} label={item.get('label')} style={buttonStyle} labelStyle={buttonLabelStyle}
                        secondary={preview.get('benefits').includes(item.get('id'))}
                        onTouchTap={() => {actions.updateArrayField('benefits', item.get('id'))}}/>
            )}
            <Divider />
            <h3>Describe your company</h3>
            <TextField defaultValue={preview.get('aboutCompany')} multiLine={true} fullWidth={true} 
                       inputStyle={inputStyle} underlineStyle={{visibility: 'hidden'}} 
                       onChange={(e) => { actions.updateStringField('aboutCompany', e.target.value); }} /> 

            <h3>Describe the job</h3>
            <TextField defaultValue={preview.get('aboutJob')} multiLine={true}  fullWidth={true}
                       inputStyle={inputStyle} underlineStyle={{visibility: 'hidden'}}
                       onChange={(e) => { actions.updateStringField('aboutJob', e.target.value); }} /> 
          </div>

          <div style={previewStyle}>
            <h1>{preview.get('title')}</h1>
            {preview.get('location')}

            <h2>About the company</h2>
            {preview.get('aboutCompany')}

            <h2>About the job</h2>
            {preview.get('aboutJob')}

            <h4>Skills Required</h4>
            <ul>
              {preview.get('skills').map((id, index) => 
                <Flipper key={index} builder={builder}
                         line={builder.getIn(['allSkills', id, 'template'])} />
              )}
            </ul>

            <h4>Benefits</h4>
            <ul>
              {preview.get('benefits').map((id, index) => 
                <Flipper key={index} builder={builder}
                         line={builder.getIn(['allBenefits', id, 'template'])} />
              )}
            </ul>
          </div>
        </div>
      </div>);
  }

}

MainSection.propTypes = {
  preview: PropTypes.object.isRequired,
  builder: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
}

MainSection.childContextTypes = {
  muiTheme: React.PropTypes.object
};

