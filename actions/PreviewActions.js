import * as types from '../constants/ActionTypes';

export function updateStringField(key, value) {
  return {
    type: types.UPDATE_STRING_FIELD,
    key: key,
    value: value
  };
}

export function updateArrayField(key, value) {
  return {
    type: types.UPDATE_ARRAY_FIELD,
    key: key,
    value: value
  };
}
